import socket
import numpy as np
import math
import matplotlib.pyplot as plt


server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(("", 8000))
server.listen(1)

# bucle para atender clientes
while 1:
# Se espera a un cliente
    socket_cliente, datos_cliente = server.accept()
# Se escribe su informacion
    print ("conectado "+str(datos_cliente))

# Bucle indefinido hasta que el cliente envie "adios"
    seguir = True
    #while seguir:
        # Espera por datos
    peticion = (socket_cliente.recv(1000)).decode()
    fx = lambda x: eval(peticion)
    aux2 = "recivido"

    socket_cliente.send(aux2.encode())
    aux = (socket_cliente.recv(1000)).decode()
    socket_cliente.send(aux2.encode())
    aux2 = (socket_cliente.recv(1000)).decode()
    socket_cliente.send(aux2.encode())
    aux3 = (socket_cliente.recv(1000)).decode()

    a = int(aux2)
    b = int(aux3)
    tramos = int(aux)

    # PROCEDIMIENTO
    # Regla del Trapecio
    # Usando tramos equidistantes en intervalo

    muestras = tramos + 1
    xi = np.linspace(a,b,muestras)
    fi = fx(xi)

    # Regla del Trapecio
    # Usando puntos muestreados
    # incluso arbitrariamente espaciados
    suma = 0
    lista_areas=[]
    aux=0
    for i in range(0,tramos,1):
        dx = xi[i+1]-xi[i]
        Atrapecio = dx*(fi[i]+fi[i+1])/2
        lista_areas.append(Atrapecio)
        suma = suma + Atrapecio
    integral = suma
   

    # SALIDA
    #print('tramos: ', tramos)
    #print('Integral: ', area)
    socket_cliente.send(str(integral).encode())

    peticion2 = (socket_cliente.recv(1000)).decode()
    if peticion2 == "recivido":

        # GRAFICA
        # Puntos de muestra
        muestras = tramos + 1
        xi = np.linspace(a,b,muestras)
        fi = fx(xi)
        # Linea suave
        muestraslinea = tramos*10 + 1
        xk = np.linspace(a,b,muestraslinea)
        fk = fx(xk)
        str1=','.join(str(x) for x in xk)
        str2=','.join(str(y) for y in fk)
        str3=','.join(str(z) for z in xi)
        str4=','.join(str(q) for q in fi)
        str5=','.join(str(w) for w in lista_areas)
        socket_cliente.send(str1.encode())
        socket_cliente.send(str2.encode())
        socket_cliente.send(str3.encode())
        socket_cliente.send(str4.encode())
        socket_cliente.send(str5.encode())
        socket_cliente.send(str(muestras).encode())


        # Graficando
        plt.plot(xk,fk, label ='f(x)')
        plt.plot(xi,fi, marker='o',
                color='orange', label ='muestras')

        plt.xlabel('x')
        plt.ylabel('f(x)')
        plt.title('Integral: Regla de Trapecios')
        plt.legend()

        # Trapecios
        plt.fill_between(xi,0,fi, color='g')
        for i in range(0,muestras,1):
            plt.axvline(xi[i], color='w')

        #plt.show()
        break

        