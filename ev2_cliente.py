import socket
import time
# Se establece la conexion
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
s.connect(('localhost', 8000))

import numpy as np
import math
import matplotlib.pyplot as plt
import PySimpleGUI as sg


layout = [[sg.Text('Ingrese función')],
          [sg.Input()],
          [sg.Text('Ingrese número de trapecios que desea usar')],
          [sg.Input()],
          [sg.Text('Ingrese punto A')],
          [sg.Input()],
          [sg.Text('Ingrese punto B')],
          [sg.Input()],
                    [sg.OK()] ]

event, values = sg.Window('Presione ok para continuar', layout).Read()
sg.Popup(event, values[0])

# Se envia "hola"
aux = str(values[0])
s.send(aux.encode())
# Se recibe la respuesta 

datos = s.recv(1000)
aux2 = str(values[1])
s.send(aux2.encode())


datos2 = s.recv(1000)
aux3 = str(values[2])
s.send(aux3.encode())


datos3 = s.recv(1000)
aux4 = str(values[3])
s.send(aux4.encode())


# Se recibe la respuesta y se escribe en pantalla
datos4 = s.recv(1000)
print (datos4)
aux5 = "recivido"
s.send(aux5.encode())
#grafico
xk = s.recv(1000)
fk = s.recv(1000)
xi = s.recv(1000)
fi = s.recv(1000)
lista_areas = s.recv(1000)
muestras = int(s.recv(1000))


plt.plot(eval(xk),eval(fk), label ='f(x)')
plt.plot(eval(xi),eval(fi), marker='o',
        color='orange', label ='muestras')

plt.xlabel('x')
plt.ylabel('f(x)')
plt.title('Integral: Regla de Trapecios')
#plt.legend()

# Trapecios
plt.fill_between(eval(xi),0,eval(fi), color='g')
for i in range(0,muestras,1):
    plt.axvline(eval(xi)[i], color='w')

plt.show()
print(eval(lista_areas))
s.close()